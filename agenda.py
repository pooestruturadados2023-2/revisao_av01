#Crie uma classe Agenda que pode armazenar 10 pessoas e que seja capaz de realizar as seguintes operações: ```
#armazenaPessoa(nome: str, idade: int, altura: float) // guarda o objeto pessoa na lista
#removePessoa(nome: str) // remove a pessoa da lista
#buscaPessoa(nome: str) // informa em que posição da agenda está a pessoa
#imprimeAgenda() // imprime os dados de todas as pessoas da agenda
#imprimePessoa(index: str) // imprime os dados da pessoa que está na posição“i” da agenda. ```

from pessoa import Pessoa

class Agenda:
    def __init__(self):
        self.__lista=[]
    
    def armazena_pessoa(self,p:Pessoa):
        assert isinstance(p,Pessoa), 'O objeto adicionado deve ser um objeto do tipo Pessoa'
        self.__lista.append(p)
    
    def remove_pessoa(self,p):
        posicao=self.busca_pessoa(p)
        if str(posicao).isnumeric():
            self.__lista[posicao]=False
            self.__lista.remove(False)
        else: print(posicao)
    
    def busca_pessoa(self,n):
        for index,pessoa in enumerate(self.__lista):
            if pessoa.nome == n:
                return index
        return f'{n} nao enconstrado!'
    
    def imprime_agenda(self):
        if self.__lista != []:
            for pessoa in self.__lista:
                print(pessoa)
        else: print('Lista vazia!')
        
    def imprime_pessoa(self,p):
        assert str(p).isnumeric() and -1 < p < len(self.__lista), 'A posicao precisa ser um numero que esteja na lista'
        print(self.__lista[p])

if __name__ == '__main__':
    a=Agenda()
    p=Pessoa('Joao','15/12/2003',1.76)
    p1=Pessoa('Carinha','27/11/2009',1.71)
    p2=Pessoa('Pessoinha','06/07/2008',1.70)
    
    a.imprime_agenda()
    a.armazena_pessoa(p)
    a.armazena_pessoa(p1)
    a.armazena_pessoa(p2)
    a.imprime_pessoa(2)
    a.remove_pessoa('lala')
    print('Posicao:', a.busca_pessoa('Joao'))
    a.imprime_pessoa(a.busca_pessoa('Joao'))
    a.remove_pessoa('Joao')
    #a.imprime_pessoa(a.busca_pessoa('Joao'))
    print('Posicao:', a.busca_pessoa('Joao'))
    a.imprime_agenda()