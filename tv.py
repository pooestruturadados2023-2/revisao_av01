#Crie uma classe Televisao e uma classe ControleRemoto que pode controlar o volume e trocar os canais da televisão. 
#O controle de volume permite:
#aumentar ou diminuir a potência do volume de som em uma unidade de cada vez;
#aumentar e diminuir o número do canal em uma unidade
#trocar para um canal indicado;
#consultar o valor do volume de som e o canal selecionado.

class Televisao:
    def __init__(self):
        self.__volume=0
        self.__canal=1
    
    def receber_comando(self,c):
        if c in ['+', '-']:
            if c == '+' and self.__volume <= 90:
                self.__volume+=10
                print('volume acrescentado')
            elif c == '-' and self.__volume > 0:
                self.__volume-=10
                print('volume reduzido')
        elif c in ['p', 'a']:
            if c == 'p' and self.__canal < 98:
                self.__canal+=1
                print('indo para proximo canal')
            elif c == 'a' and self.__canal > 1:
                self.__canal-=1
                print('indo para canal anterior')
        elif isinstance(c,int):
            self.__canal=c
            print(f'indo para canal {c}')
        elif c == 'info':
            print(f'''
Informacoes da TV
Canal atual: {self.__canal}                  
Volume: {self.__volume}
''')
            
class ControleRemoto:
    def ajustar_volume(self,a):
        assert a in ['+', '-'],'opcao invalida!'
        return a
    
    def passar_canal(self,a):
        assert a in ['p', 'a'],'opcao invalida!'
        return a
    
    def trocar_canal(self,c):
        assert isinstance(c,int) and 0 < c < 100, 'O valor precisar ser um numero entre 1 e 99'
        return c
    
    def verificar_info(self):
        return 'info'
    
if __name__ == '__main__':
    tv=Televisao()
    c=ControleRemoto()
    tv.receber_comando(c.verificar_info())
    tv.receber_comando(c.ajustar_volume('+'))
    tv.receber_comando(c.verificar_info())
    tv.receber_comando(c.ajustar_volume('+'))
    tv.receber_comando(c.verificar_info())
    tv.receber_comando(c.ajustar_volume('-'))
    tv.receber_comando(c.verificar_info())
    tv.receber_comando(c.passar_canal('p'))
    tv.receber_comando(c.passar_canal('p'))
    tv.receber_comando(c.passar_canal('p'))
    tv.receber_comando(c.passar_canal('p'))
    tv.receber_comando(c.passar_canal('p'))
    tv.receber_comando(c.passar_canal('p'))
    tv.receber_comando(c.verificar_info())
    tv.receber_comando(c.passar_canal('a'))
    tv.receber_comando(c.passar_canal('a'))
    tv.receber_comando(c.verificar_info())
    tv.receber_comando(c.trocar_canal(75))
    tv.receber_comando(c.verificar_info())
    
    