#Crie uma classe para representar uma pessoa, com os atributos privados de nome, data de nascimento e altura. 
#Crie os métodos públicos necessários para sets e gets e também um método para imprimir todos dados de uma pessoa.
#Crie um método para calcular a idade da pessoa.

from datetime import datetime
from math import floor

class Pessoa:
    def __init__(self,nome,dtnasc,altura):
        self.nome=nome
        self.dtnasc=dtnasc
        self.altura=altura
    
    @property
    def nome(self):
        return self.__nome
    
    @nome.setter
    def nome(self,n):
        self.__nome=n
    
    @property
    def dtnasc(self):
        return self.__dtnasc
    
    @dtnasc.setter
    def dtnasc(self,d):
        assert len(d) == 10, 'A data deve seguir o formato "DD/MM/AAAA"!'
        assert d[:2].isnumeric() and d[2]=='/' and d[3:5].isnumeric() and d[5]=='/' and d[6:].isnumeric(),\
'A data deve seguir o formato "DD/MM/AAAA"!'
        self.__dtnasc=d
    
    @property
    def altura(self):
        return self.__altura
    
    @altura.setter
    def altura(self,a):
        self.__altura=a
    
    def calcula_idade(self):
        data=datetime.strptime(f'{self.dtnasc[6:]}-{self.dtnasc[3:5]}-{self.dtnasc[:2]}','%Y-%m-%d')
        data=f'{floor(float(str(abs((data-datetime.today()).days)/365.25)))}'
        return data
    
    def __str__(self):
        return f'''
Nome: {self.nome}
Data nascinemtnto: {self.dtnasc}
Altura: {self.altura}m
Idade: {self.calcula_idade()} anos
'''

if __name__ == '__main__':
    pessoa=Pessoa('Joao', '15/12/2003',1.76)
    print(pessoa.calcula_idade())
    print(pessoa)
